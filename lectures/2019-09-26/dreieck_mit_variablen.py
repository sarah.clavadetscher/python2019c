from turtle import *
reset()

laenge = numinput("Dreieck", "Länge des Dreiecks")
farbe = textinput("Dreieck", "Farbe Ihres Dreiecks")
füllfarbe = textinput("Dreieck", "Füllfarbe")

shape("turtle")
pensize(5)
pencolor(farbe)
fillcolor(füllfarbe)

begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()

exitonclick()
